package team.r.iothing.util.services;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Handler;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import team.r.iothing.data.model.Accelerometer;
import team.r.iothing.data.model.SensorDatum;
import team.r.iothing.data.networking.endpoints.AccelerometerApi;
import team.r.iothing.data.networking.utils.ApiUtils;

public class AccelerometerService extends Service implements SensorEventListener {

    private SensorManager mSensorManager;
    private static final double grav = 9.4;
    private Double stackedValue = new Double(0.0);
    private Handler handler = new Handler();
    private String deviceId = "";
    private String deviceName = "";
    private AccelerometerApi accelerometerApi;

    public AccelerometerService() {}

    public double round(double d) {
        return (long) (d * 1e2) / 1e2;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
        //registering Sensor
        Sensor sensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);

        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

        return Service.START_STICKY;
    }

    private String getDeviceId(){
        return Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    private String getDeviceName(){
        return android.os.Build.MODEL;
    }

    @Override
    public void onCreate() {
        deviceId = getDeviceId();
        deviceName = getDeviceName();
        accelerometerApi = ApiUtils.getAPIService();
        //uploadAccelerometerData();

        handler.post(runnableCode);
        mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
    }

    @Override
    public IBinder onBind(Intent intent) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() != Sensor.TYPE_ACCELEROMETER)
            return;
        float x = event.values[0];
        float y = event.values[1];
        float z = event.values[2];
        if(x > 0.1 || y > 0.1 || z < 9)
            stackedValue = Math.abs(round(Math.sqrt(x*x + y*y + z*z) - grav));
    }

    public void uploadAccelerometerData() {
        Accelerometer accelerometer = new Accelerometer();
        List<SensorDatum> sensorData = new ArrayList<>();
        SensorDatum listItem = new SensorDatum();

        //set times
        SimpleDateFormat simpleDate =  new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
        simpleDate.setTimeZone(TimeZone.getTimeZone("GMT+1"));

        //set request body
        listItem.setDate(simpleDate.format(Calendar.getInstance().getTime()));
        listItem.setDeviceName(deviceName);
        listItem.setSpeed(stackedValue);

        sensorData.add(listItem);
        accelerometer.setSensorData(sensorData);

        accelerometerApi.postAccelerometerData(deviceId, accelerometer).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                Log.i(this.getClass().getSimpleName(), "Request successful");
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                Log.e(this.getClass().getSimpleName(), "Unable to submit post to API.");
            }
        });
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    private Runnable runnableCode = new Runnable() {
        @Override
        public void run() {
            int seconds = Calendar.getInstance().getTime().getSeconds();
            System.out.println(seconds);
            if(seconds == 30 || seconds == 0){
                uploadAccelerometerData();
            }
            handler.postDelayed(this, 1000);
        }
    };
}