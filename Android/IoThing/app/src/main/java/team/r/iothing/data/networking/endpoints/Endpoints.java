package team.r.iothing.data.networking.endpoints;

public class Endpoints {
    public static class Accelerometer{
        public static String create(String deviceId){
            return "accelerometer/"+deviceId;
        }
    }
}
