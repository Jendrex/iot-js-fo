package team.r.iothing.ui;

import android.content.Context;

import team.r.iothing.util.NetworkHelper;
import team.r.iothing.util.enums.ToggleState;
import team.r.iothing.util.services.AccelerometerService;

public class MainActivityPresenter {

    private MainActivityView mAView;

    public MainActivityPresenter(MainActivityView mAView){
        this.mAView = mAView;
    }

    public void viewIsVisible(Context context) {
        refreshView(context);
    }

    public void refreshButtonClicked(Context context) {
        refreshView(context);
    }

    private void refreshView(Context context) {
        mAView.toggleProgressBar(ToggleState.ENABLE);
        NetworkHelper newNetworkHelper = NetworkHelper.getInstance();
        mAView.toggleProgressBar(ToggleState.DISABLE);
        if(newNetworkHelper.isNetworkAvailable(context)){
            mAView.showOkStatusImageWithMessage("Service is up.");
        }else{
            mAView.showWarningStatusImageWithMessage("No internet connection detected.");
        }
    }


}
