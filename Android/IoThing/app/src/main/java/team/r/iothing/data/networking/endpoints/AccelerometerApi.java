package team.r.iothing.data.networking.endpoints;


import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import retrofit2.http.Path;
import team.r.iothing.data.model.Accelerometer;

public interface AccelerometerApi {

    @POST("api/accelerometer/{deviceId}")
    Call<Void> postAccelerometerData(@Path("deviceId") String deviceId, @Body Accelerometer accelerometer);
}