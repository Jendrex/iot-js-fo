package team.r.iothing.util.enums;

public enum ToggleState {
    ENABLE,
    DISABLE
}
