package team.r.iothing.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SensorDatum {

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("speed")
    @Expose
    private Double speed;

    @SerializedName("device_name")
    @Expose
    private String deviceName;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public Double getSpeed() {
        return speed;
    }

    public void setSpeed(Double speed) {
        this.speed = speed;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }
}