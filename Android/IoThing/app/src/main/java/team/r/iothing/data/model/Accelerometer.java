package team.r.iothing.data.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Accelerometer {

    @SerializedName("sensor_data")
    @Expose
    private List<SensorDatum> sensorData = null;

    public List<SensorDatum> getSensorData() {
        return sensorData;
    }

    public void setSensorData(List<SensorDatum> sensorData) {
        this.sensorData = sensorData;
    }
}