package team.r.iothing.ui;

import team.r.iothing.util.enums.ToggleState;

public interface MainActivityView {

    void showOkStatusImageWithMessage(String message);

    void showWarningStatusImageWithMessage(String message);

    void toggleProgressBar(ToggleState toggleState);
}
