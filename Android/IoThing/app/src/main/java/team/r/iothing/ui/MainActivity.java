package team.r.iothing.ui;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import team.r.iothing.R;
import team.r.iothing.util.enums.ToggleState;
import team.r.iothing.util.services.AccelerometerService;

public class MainActivity extends AppCompatActivity implements MainActivityView {

    private MainActivityPresenter mAPresenter;
    private ImageView statusImageView;
    private ProgressBar progressBar;
    private TextView stateTitleTextView;
    private TextView stateMessageTextView;
    private ConstraintLayout contentLayout;
    private Button refreshButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mAPresenter = new MainActivityPresenter(this);
        statusImageView = findViewById(R.id.statusImageView);
        progressBar = findViewById(R.id.progressBar);
        contentLayout = findViewById(R.id.contentLayout);
        stateTitleTextView = findViewById(R.id.stateTitleTextView);
        stateMessageTextView = findViewById(R.id.stateMessageTextView);
        refreshButton = findViewById(R.id.refreshButton);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAPresenter.viewIsVisible(getApplicationContext());
        if(!isServiceRunning(AccelerometerService.class)) {
            startService(new Intent(this, AccelerometerService.class));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopService(new Intent(this, AccelerometerService.class));
    }

    public void refreshButtonClicked(View view) {
        mAPresenter.refreshButtonClicked(getApplicationContext());
    }

    //View methods:
    ///////////////////////////////////////////////////////////////////////////////////////////////
    @Override
    public void showOkStatusImageWithMessage(String message) {
        statusImageView.setBackgroundResource(R.drawable.ok);
        stateTitleTextView.setText("Success");
        stateMessageTextView.setText(message);
        refreshButton.setVisibility(View.INVISIBLE);
    }

    @Override
    public void showWarningStatusImageWithMessage(String message) {
        statusImageView.setBackgroundResource(R.drawable.warning);
        stateTitleTextView.setText("Warning");
        stateMessageTextView.setText(message);
        refreshButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void toggleProgressBar(ToggleState toggleState) {
        switch (toggleState){
            case ENABLE:
                hideContent();
                progressBar.setVisibility(View.VISIBLE);
                break;

            default: //DISABLE
                progressBar.setVisibility(View.INVISIBLE);
                showContent();
        }
    }


    //Private methods:
    ///////////////////////////////////////////////////////////////////////////////////////////////
    private void hideContent() {
        contentLayout.setVisibility(View.INVISIBLE);
    }

    private void showContent() {
        contentLayout.setVisibility(View.VISIBLE);
    }

    private boolean isServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }
}
