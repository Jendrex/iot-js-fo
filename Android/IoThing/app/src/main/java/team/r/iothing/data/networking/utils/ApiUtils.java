package team.r.iothing.data.networking.utils;

import team.r.iothing.data.networking.RetrofitClient;
import team.r.iothing.data.networking.endpoints.AccelerometerApi;

public class ApiUtils {

    private ApiUtils() {}

    public static final String BASE_URL = "https://iothing.000webhostapp.com";

    public static AccelerometerApi getAPIService() {
        return RetrofitClient.getClient(BASE_URL).create(AccelerometerApi.class);
    }
}