<html>
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Spy Mobile 0.7</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" type="text/css" media="screen" href="main.css" />
        <link rel="icon" href="https://www.emoji.co.uk/files/phantom-open-emojis/smileys-people-phantom/12341-sleuth-or-spy.png"/>
    </head>

   <body>
    <header>
        <h2>Spy Mobile</h2>
    </header>
    <section>
        <div id="Left_side">
            <label>Device:</label>
            <br/>
            <select id = "deviceSel" onchange = "loadGraphData()">
            </select>
            <br/>
            <label>Time span:</label>
            <br/>
            <select id = "timespan" onchange = "loadGraphData()">
                <option value=1 selected>1 hour</option>
                <option value=3>3 hours</option>
                <option value=24>1 day</option>
                <option value=168>1 week</option>
            </select>
            <br/>
        </div>

        <div id="Center_side">
            <canvas id="myChart"></canvas>
            <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
            <script type="text/javascript" >
            var chart = null;
                            
                function loadDevices(){
                    var deviceSelect = document.getElementById("deviceSel");
                    var oReq = new XMLHttpRequest();
                    oReq.onload = function() {
                        var json = JSON.parse(this.responseText);
                        for(var i = 0; i < json.length; i++) {
                            var option = document.createElement("option");
                            option.value = i;
                            option.text = json[i].name;
                            if(i == 0){
                                option.selected = true;
                            }
                            deviceSelect.appendChild(option);
                        }
                    }
                    oReq.open("get", "dataLoader.php?scope=168", false);
                    oReq.send();
                }

                function loadGraphData(){
                    //Data logic
                    var deviceName = "";
                    var datesArray = [];
                    var valuesArray = [];
                    //Request param
                    var scopeSelect = document.getElementById("timespan");
                    var scope = scopeSelect.options[scopeSelect.selectedIndex].value;
                    //Device
                    var deviceSel = document.getElementById("deviceSel");
                    var device = deviceSel.options[deviceSel.selectedIndex].text;
                    var deviceId = -1;
                    //Request
                    var oReq = new XMLHttpRequest();
                    oReq.onload = function() {
                        //ToDo: Clear past data!!
                        var json = JSON.parse(this.responseText);
                        console.log(json);
                        for(var i = 0; i < json.length; i++) {
                            if(json[i].name == device){
                                deviceId = i;
                            }
                        }
                        if(deviceId == -1){
                            var ctx = document.getElementById('myChart').getContext('2d');
                            if(chart != null){
                                chart.destroy();
                            }
                            chart = new Chart(ctx, {
                            type: 'line',
                            data: {
                                labels: ["No data"],
                                datasets: [{
                                labels:'No data',
                                backgroundColor: ['#D3D3D3'],
                                data: [100],
                                }]
                            },
                            options: {}
                            });
                        }
                        else{
                            deviceName = json[deviceId].name;
                            for(var i = 0; i < json[deviceId].values.length; i++) {
                                valuesArray[i] = json[deviceId].values[i];
                                datesArray[i] = json[deviceId].dates[i];
                            }
                            //Graph logic
                            var ctx = document.getElementById('myChart').getContext('2d');
                            if(chart != null){
                                chart.destroy();
                            }
                            chart = new Chart(ctx, {
                            // The type of chart we want to create
                            type: 'line',

                            // The data for our dataset
                            data: {
                                labels: datesArray,
                                datasets: [{
                                    label: deviceName,
                                    backgroundColor: 'rgb(255, 99, 132)',
                                    borderColor: 'rgb(255, 99, 132)',
                                    data: valuesArray,
                                    // spanGaps: true,
                                }]
                            },
                            // Configuration options go here
                            options: {
                                // animation: {
                                //     duration: 0, // general animation time
                                // },
                                // hover: {
                                //     animationDuration: 0, // duration of animations when hovering an item
                                // },
                                // responsiveAnimationDuration: 0, // animation duration after a resize
                            }
                            });
                        }  
                    };
                    oReq.open("get", "dataLoader.php?scope="+scope, true);
                    oReq.send();
                }
                loadDevices();
                loadGraphData();
                //Uncomment once front is nearly finished for 30sec refreshing of graphs
                setInterval(loadGraphData, 30000);
            </script>
        </div>
    </section>
    <footer>
        <span>Copyright @FO, JS</span>
    </footer>
   </body>
</html>