<?php
require_once 'flight/Flight.php';

//Set custom 404
Flight::map('notFound', function(){
    $response = array('error_info' => 'The requested resource could not be found.', 'status_code' => 404);
    Flight::halt(404, json_encode($response));
});

Flight::route('POST /accelerometer/@deviceId:.{1,64}', function($deviceId) {

    $sensorData = Flight::request()->data->sensor_data;
    $sensorDataRows = count($sensorData);
    $httpCode = 202;

    if($sensorDataRows == 0) {
        $httpCode = 400;
        $response = array('error_info' => 'Bad request.', 'status_code' => $httpCode);
        Flight::halt($httpCode, json_encode($response));
    }

    require_once 'connect.php';

    $query = "INSERT INTO `sensor_data`(`date`, `speed`, `device_id`, `device_name`) ";
    foreach($sensorData as $dataInstance) {
        mysqli_query($connection, $query."VALUES ('".$dataInstance['date']."','".$dataInstance['speed']."','".$deviceId."','".$dataInstance['device_name']."')");
    }
    
    mysqli_close($connection);
});

Flight::route('GET /accelerometer/@timeInHours:[0-1]{0,1}[0-9]{1,2}(/@deviceId:.{1,64})', function($timeInHours, $deviceId) {
    $timeLimiter = date("Y-m-d H:i:s", strtotime('-'.$timeInHours.' hours'));

    require_once 'connect.php';

    $deviceIdFilter = "";
    if(isset($deviceId)){
        $deviceIdFilter = " AND `device_id` = '$deviceId'";
    }
    $query = "SELECT `device_name`, `speed`, `date` FROM `sensor_data` WHERE `date` >= '$timeLimiter'".$deviceIdFilter;
    if ($result = mysqli_query($connection, $query)) {
        $rows = array();
        $deviceNames = array();
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
            $deviceNames[] = $row['device_name'];
        }
        $deviceNames = array_unique($deviceNames);
        if (!empty($rows)){
            $jsonArray = array();
            foreach($deviceNames as $deviceName){
                $values = array();
                $dates = array();
                foreach($rows as $row){
                    if($row['device_name'] == $deviceName){
                        $values[] = $row['speed'];
                        $dates[] = $row['date'];
                    }
                }
                $outputArray['name'] = $deviceName;
                $outputArray['values'] = $values;
                $outputArray['dates'] = $dates;
                $jsonArray[] = $outputArray;
            }
        }
        print json_encode($jsonArray);
        mysqli_free_result($result);
    }
    mysqli_close($connection);
});

Flight::start();
?>